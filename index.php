<!DOCTYPE html>
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include("header.php") ?>
<div class="row">
  <div class="col-12">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="/img/2.jpg" width="400" height="600" alt="Первый слайд">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="/img/1.jpg" width="400" height="600" alt="Второй слайд">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="/img/3.jpg" width="400" height="600" alt="Третий слайд">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
</div>
<div class="row">
    <div class="col-4">
      <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Автоматическое SEO-продвижение</h5>
          <p class="card-text">Управление ав­то­ма­ти­зи­ро­ван­ным про­дви­же­ни­ем в поисковых системах.</p>
          <a href="#" class="btn btn-primary">Подробнее</a>
        </div>
      </div>
    </div>
    <div class="col-4">
      <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">SSL-сертификаты</h5>
          <p class="card-text">Гарантия статуса сайта и безопасности передаваемых данных. Незаменимо для e-commerce.</p>
          <a href="#" class="btn btn-primary">Подробнее</a>
        </div>
      </div>
    </div>
    <div class="col-4">
      <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Хостинг и серверы</h5>
          <p class="card-text">Надёжный классический и VIP хостинг, VPS с SSD+HDD и SSD носителями, Dedicated. Домены в подарок.</p>
          <a href="#" class="btn btn-primary">Подробнее</a>
        </div>
      </div>
    </div>
</div>

  <div class="col">
          1 из 3
  </div>
    <div class="col-6">
          2 из 3 (широкая)
    </div>
      <div class="col">
          3 из 3
      </div>

      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src="/js/script.js"></script>
  <?php include ("footer.php") ?>
